const { RESTDataSource } = require('apollo-datasource-rest');

const serverConfig = require('../server');

class CareServiceAPI extends RESTDataSource {
    constructor() {
        super();
        this.baseURL = serverConfig.careService_api_url;
    }

    async createCareService(careservice){
        careservice = new Object(JSON.parse(JSON.stringify(careservice)));
        return await this.post(`/asistencias`, careservice);
    }

    async careServiceById(careserviceId) {
        return await this.get(`/asistencias/${careserviceId}`);
    }

    async updateCareService(careServiceById){        
        return await this.put(`/asistencias/${careServiceById}`);
    }

    async removeCareService(careServiceById){
        return await this.delete(`asistencias/${careServiceById}` );
    }
}

    module.exports = CareServiceAPI;    

