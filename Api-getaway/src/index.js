
const { ApolloServer } = require('apollo-server');

const typeDefs = require('./typeDefs');
const resolvers = require('./resolvers');
const CareServiceAPI = require('./dataSources/careService_api');
const UserAPI = require('./dataSources/user_api');
const authentication = require('./utils/authentication');

const server = new  ApolloServer({
    context: authentication,
    typeDefs,
    resolvers,
    dataSources : () => ({
        careserviceAPI: new CareServiceAPI(),
        userAPI: new UserAPI(),
    }),
    introspection: true,
    playground: true
});

server.listen(process.env.PORT || 4000).then(({ url }) => {
    console.log(`🚀 Server ready at ${url}`);
});