
const careServiceResolver = {
    Query : {
        careServiceById: async(_, { idCareService }, { dataSources }) => {            
            return await dataSources.careserviceAPI.careServiceById(idCareService);
        }
    },
    Mutation : {
        createCareService: async(_, { careservice }, { dataSources}) => {            
            return dataSources.careserviceAPI.createCareService(careservice);            
        },
        updateCareService: async(_, { idCareService }, { dataSources}) => {
            return await dataSources.careserviceAPI.updateCareService(idCareService);            
        },
        removeCareService: async(_, { idCareService }, { dataSources}) => {
                return await dataSources.careserviceAPI.removeCareService(idCareService);
        }
    }
};

module.exports = careServiceResolver;