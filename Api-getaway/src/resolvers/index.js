const careServiceResolver = require('./careService_resolver');
const usersResolver = require('./user_resolver');

const lodash = require('lodash');

const resolvers = lodash.merge(usersResolver, careServiceResolver);

module.exports = resolvers;
