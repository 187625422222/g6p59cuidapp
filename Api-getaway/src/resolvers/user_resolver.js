const usersResolver = {
    Query : {
        userDetailById: (_, { userId }, { dataSources, userIdToken }) => {
            if (userId == userIdToken) 
                return dataSources.userAPI.getUser(userId);
            else
                return dataSources.userAPI.getUser(userId);
        },
    },

    Mutation: {
        signUpUser: async(_, { userInput }, { dataSources}) => {
            const authInput = {
                idUser: userInput.idUser,
                username: userInput.username,
                password: userInput.password,
                name: userInput.name,
                lastname: userInput.lastname,
                gender: userInput.gender,
                dateOfBirth: userInput.dateOfBirth,
                movil: userInput.movil,
                telephone: userInput.telephone,
                email: userInput.email,
                department: userInput.department,
                municipality: userInput.municipality,
                addresshome: userInput.addresshome,
                applicant: userInput.applicant,
                offeror: userInput.offeror
            }
            return await dataSources.userAPI.createUser(authInput);        
        },
        logIn: (_, { credentials }, { dataSources }) =>
            dataSources.userAPI.authRequest(credentials),
        refreshToken : (_, { refresh }, { dataSources }) =>
            dataSources.userAPI.refreshToken(refresh),       
    }
};

module.exports = usersResolver;