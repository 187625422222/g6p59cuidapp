const { gql } = require('apollo-server');

const careService_TypeDefs = gql `
    type CareService {
        idCareService: String!
        userOferente: String!
        userSolicitante: String! 
        fechaServicio: String!
        duracionServicio: String!
        tarifaServicio: String!
        acuerdoPago: String!
        detalleServicio: String!        
        direccionServicio: String!
        videoAsistido: String!
    }

    input CareServiceInput {        
        duracionServicio: String!
        tarifaServicio: String!
        acuerdoPago: String!
        detalleServicio: String!
    }

    extend type Query {
        careServiceById(idCareService: String!): CareService
    }

    extend type Mutation {
        createCareService(careservice: CareServiceInput!): CareService
        updateCareService(idCareService: String!) : CareService
        removeCareService(idCareService: String!) : CareService
    }
`;
    
module.exports = careService_TypeDefs;
