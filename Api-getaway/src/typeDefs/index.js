const careService_TypeDefs = require('./careService_type_defs')
const userTypeDefs = require('./user_type_defs');

const schemasArrays = [userTypeDefs, careService_TypeDefs];

module.exports = schemasArrays;