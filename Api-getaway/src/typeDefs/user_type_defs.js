const { gql } = require('apollo-server');
const userTypeDefs = gql `
    type Tokens {
        refresh: String!
        access: String!
    }
    type Access {
        access: String!
    }
    input CredentialsInput {
        username: String!
        password: String!
    }
    input SignUpInput {
        idUser: String!
        username: String!
        password: String!
        name: String
        lastname: String
        gender: String
        dateOfBirth: String
        movil: String
        telephone: String
        email: String
        department: String
        municipality: String
        addresshome: String
        applicant: Int
        offeror: Int
       
    }
    type UserDetail {
        idUser: Int
        username: String
        password: String
        name: String
        lastname: String
        gender: String
        dateOfBirth: String
        movil: String
        telephone: String
        email: String
        department: String
        municipality: String
        addresshome: String
        applicant: Int
        offeror: Int
    }
    type Mutation {
        signUpUser(userInput :SignUpInput): Tokens!
        logIn(credentials: CredentialsInput!): Tokens!
        refreshToken(refresh: String!): Access!
    }
    type Query {
        userDetailById(userId: Int!): UserDetail!
    }
    `;

    module.exports = userTypeDefs;