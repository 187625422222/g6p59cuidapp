package com.misiontic.service_care;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceCareApplication {

	public static void main(String[] args) {
	try {
		SpringApplication.run(ServiceCareApplication.class, args);
	} catch (Exception e) { e.printStackTrace(); }
	}
}
