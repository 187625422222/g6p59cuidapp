package com.misiontic.service_care.controllers;

import com.misiontic.service_care.exceptions.CareServiceNotFoundException;
import com.misiontic.service_care.models.CareService;
import com.misiontic.service_care.repositories.careServiceRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
//import java.util.List;
import java.util.Optional;
//import java.util.stream.Collectors;
//import java.util.stream.Stream;

@RestController
public class CareServiceController {

    private final careServiceRepository theServiceRepository;

    public CareServiceController (careServiceRepository theServiceRepository){
        this.theServiceRepository = theServiceRepository;
    }

    @PostMapping("/asistencias")
    CareService newCareService (@RequestBody CareService careService ){
        return theServiceRepository.save(careService);
    }

    @GetMapping("/asistencias/{id}")
    CareService getCareService (@PathVariable ("id") String idCareService) {
        return theServiceRepository.findById(idCareService).orElseThrow(() -> new CareServiceNotFoundException("No se encontro la asistencia con este id" + idCareService));
    }


    @PutMapping("/asistencias/{id}")
    public ResponseEntity<CareService> updateCareService (@PathVariable ("id") String idCareService, @RequestBody CareService careService) {
        Optional<CareService> careServiceData = theServiceRepository.findById(idCareService);

        if (careServiceData.isPresent()) {
            CareService _careService = careServiceData.get();
            _careService.setFechaServicio(careService.getFechaServicio());
            _careService.setDuracionServicio(careService.getDuracionServicio());
            _careService.setTarifaServicio(careService.getTarifaServicio());
            _careService.setAcuerdoPago(careService.getAcuerdoPago());
            _careService.setDetalleServicio(careService.getDetalleServicio());
            _careService.setDireccionServicio(careService.getDireccionServicio());
            _careService.setVideoAsistido(careService.getVideoAsistido());
            _careService.setUserOferente(careService.getUserOferente());
            _careService.setUserSolicitante(careService.getUserSolicitante());
            return new ResponseEntity<>(theServiceRepository.save(_careService), HttpStatus.OK);
        } else { return new ResponseEntity<>(HttpStatus.NOT_FOUND);}
    }

    @DeleteMapping("/asistencias/{id}")
    public ResponseEntity<HttpStatus> removeCareService (@PathVariable("id") String idCareService){
        try {
            theServiceRepository.deleteById(idCareService);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception e){ return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);}
    }


    /*
    @GetMapping("/careservices/{oferente}")
    List<CareService> servicioUser(@PathVariable Integer idCareService) {
        CareService elServicioEsteOferente = theServiceRepository.findById(idCareService).orElse(null);

        if (elServicioEsteOferente == null)
          throw new CareServiceNotFoundException("el oferente no ha publicado un servicio: " + idCareService);

        List<CareService> serviciosOferente = theServiceRepository.findByUserOferente(idCareService);
        List<CareService> servicios = Stream.concat(serviciosOferente.stream(),serviciosSolicitante.stream()).collect(Collectors.toList());
        return servicios;
    }

    @GetMapping("/careservices/{solicitante}")
    List<CareService> servicioUser(@PathVariable Integer idCareService) {

        areService elServicioEsteSolicitante = theServiceRepository.findById(idCareService).orElse(null);
        if (elServicioEsteSolicitante == null)
            throw new CareServiceNotFoundException("el solicitante no ha requerido del servicio: " + idCareService);

        List<CareService> serviciosSolicitante = theServiceRepository.findByUserSolicitante(idCareService);
        List<CareService> servicios = Stream.concat(serviciosOferente.stream(),serviciosSolicitante.stream()).collect(Collectors.toList());
        return servicios;
    }
    */
}
