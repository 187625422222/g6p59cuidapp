package com.misiontic.service_care.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@ControllerAdvice
@ResponseBody

public class CareServiceNotFoundAdvice {
    @ResponseBody
    @ExceptionHandler(CareServiceNotFoundException.class)
    @ResponseStatus (HttpStatus.NOT_FOUND)
    String EntityNotFoundAdvice (CareServiceNotFoundException ex){
        return ex.getMessage();
    }
}
