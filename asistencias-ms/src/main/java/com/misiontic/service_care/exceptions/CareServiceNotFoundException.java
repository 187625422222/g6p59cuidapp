package com.misiontic.service_care.exceptions;

public class CareServiceNotFoundException extends RuntimeException{
    public  CareServiceNotFoundException (String message) { super(message); }
}
