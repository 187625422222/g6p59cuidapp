
package com.misiontic.service_care.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "asistencias")
public class CareService {
    @Id
    private String idCareService;
    private String userSolicitante;
    private String userOferente;
    private Date fechaServicio;
    private String duracionServicio;
    private String tarifaServicio;
    private String acuerdoPago;
    private String detalleServicio;
    private String direccionServicio;
    private String videoAsistido;

    public CareService(String idCareService, String userSolicitante,
                       String userOferente, Date fechaServicio,
                       String direccionServicio, String tarifaServicio,
                       String acuerdoPago, String detalleServicio,
                       String duracionServicio, String videoAsistido)
    {
        this.idCareService = idCareService;
        this.userSolicitante = userSolicitante;
        this.userOferente = userOferente;
        this.fechaServicio = fechaServicio;
        this.direccionServicio = direccionServicio;
        this.tarifaServicio = tarifaServicio;
        this.acuerdoPago = acuerdoPago;
        this.detalleServicio = detalleServicio;
        this.duracionServicio = duracionServicio;
        this.videoAsistido = videoAsistido;
    }

    public String getIdCareService() {
        return idCareService;
    }

    public void setIdCareService(String idCareService) {
        this.idCareService = idCareService;
    }

    public String getUserSolicitante() {
        return userSolicitante;
    }

    public void setUserSolicitante(String userSolicitante) {
        this.userSolicitante = userSolicitante;
    }

    public String getUserOferente() {
        return userOferente;
    }

    public void setUserOferente(String userOferente) {
        this.userOferente = userOferente;
    }

    public String getDuracionServicio() {
        return duracionServicio;
    }

    public void setDuracionServicio(String duracionServicio) {
        this.duracionServicio = duracionServicio;
    }

    public String getTarifaServicio() {
        return tarifaServicio;
    }

    public void setTarifaServicio(String tarifaServicio) {
        this.tarifaServicio = tarifaServicio;
    }

    public String getAcuerdoPago() {
        return acuerdoPago;
    }

    public void setAcuerdoPago(String acuerdoPago) {
        this.acuerdoPago = acuerdoPago;
    }

    public String getDetalleServicio() {
        return detalleServicio;
    }

    public void setDetalleServicio(String detalleServicio) {
        this.detalleServicio = detalleServicio;
    }

    public String getDireccionServicio() {
        return direccionServicio;
    }

    public void setDireccionServicio(String direccionServicio) {
        this.direccionServicio = direccionServicio;
    }

    public String getVideoAsistido() {
        return videoAsistido;
    }

    public void setVideoAsistido(String videoAsistido) {
        this.videoAsistido = videoAsistido;
    }

    public Date getFechaServicio() {
        return fechaServicio;
    }

    public void setFechaServicio(Date fechaServicio) {
        this.fechaServicio = fechaServicio;
    }
}




