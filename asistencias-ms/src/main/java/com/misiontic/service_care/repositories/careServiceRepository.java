package com.misiontic.service_care.repositories;

import com.misiontic.service_care.models.CareService;
import org.springframework.data.mongodb.repository.MongoRepository;
import java.util.List;

public interface careServiceRepository extends MongoRepository<CareService, String>
{
    List<CareService> findByUserOferente(String idCareService);
    List<CareService> findByUserSolicitante(String idCareService);
}
