from django.apps import AppConfig


class CuidappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'CuidApp'
