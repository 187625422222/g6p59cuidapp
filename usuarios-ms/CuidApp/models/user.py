from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.contrib.auth.hashers import make_password

class UserManager(BaseUserManager):
    def create_user(self, username, password=None):
        """
        Creates and saves a user with the given username and password.
        """
        if not username:
            raise ValueError('Users must have an username')
        user = self.model(username=username)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password):
        """
        Creates and saves a superuser with the given username and password.
        """
        user = self.create_user(
        username=username,
        password=password,
        )
        user.is_admin = True
        user.save(using=self._db)
        return user

class User(AbstractBaseUser, PermissionsMixin):
    id = models.BigAutoField (primary_key=True)
    idUser = models.BigIntegerField('iduser')
    username = models.CharField('username', max_length = 30, unique=True)
    password = models.CharField('password', max_length = 256)
    name = models.CharField('name', max_length = 30)
    lastname = models.CharField('lastname', max_length = 30)
    gender=models.CharField('gender', max_length = 30)
    dateOfBirth = models.CharField('dateOfBirth', max_length = 30)
    movil =models.CharField('movil', max_length = 30)
    telephone = models.CharField('telephone', max_length = 30)
    email = models.EmailField('email', max_length = 100)
    department = models.CharField('departament', max_length = 30)
    municipality= models.CharField('municipality', max_length = 30)
    addresshome= models.CharField('addressHome', max_length = 30)
    applicant = models.BigIntegerField('applicant')
    offeror = models.BigIntegerField('offeror')
    def save(self, **kwargs):
        some_salt = 'mMUj0DrIK6vgtdIYepkIxN'
        self.password = make_password(self.password, some_salt)
        super().save(**kwargs)
        
    objects = UserManager()
    USERNAME_FIELD = 'username'