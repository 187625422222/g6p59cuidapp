from rest_framework import serializers
from CuidApp.models.user import User


class UserSerializer(serializers.ModelSerializer):
   
    class Meta:
        model = User
        fields = ['id','idUser', 'username', 'password', 'name','lastname', 'gender','dateOfBirth', 'movil', 'telephone',
        'email', 'department','municipality','addresshome','applicant','offeror']
   
