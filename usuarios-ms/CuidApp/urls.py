from django.conf.urls import url
from django.urls.resolvers import URLPattern
from CuidApp import views
from CuidApp.views import ApiRest

urlpatterns=[

    url(r'User',ApiRest.UsuarioRestApi),
    url(r'User/([0-9]+)$',ApiRest.UsuarioRestApi)

]