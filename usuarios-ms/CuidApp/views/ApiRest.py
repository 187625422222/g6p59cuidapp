from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser
from CuidApp.serializers import UserSerializer
from CuidApp.models import User
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework.response import Response
from rest_framework import status, views



# Create your views here.
@csrf_exempt 
def UsuarioRestApi(request, id=0):
    if request.method == 'GET':

        user = User.objects.all()
        idUserGet = request.GET.get("id")
        if idUserGet:
            user = user.filter(id = idUserGet)
        usuarioserializers = UserSerializer(user,many=True)
        return JsonResponse(usuarioserializers.data,safe=False)

   

    elif request.method == 'PUT':
        usuarioData = JSONParser().parse(request)
        usuarioinventario = User.objects.get(idUser=usuarioData['idUser'])
        usuarioserializers = UserSerializer(usuarioinventario,data=usuarioData)
        if usuarioserializers.is_valid():
            usuarioserializers.save()
            return JsonResponse("Actualizado",safe=False)
        return JsonResponse("No Actualizado",safe=False)
    elif request.method =='DELETE':
        usuarioData = JSONParser().parse(request)
        usuarioinventario = User.objects.get(idUser=usuarioData['idUser'])
        usuarioinventario.delete()
        return JsonResponse("Elimino", safe=False)