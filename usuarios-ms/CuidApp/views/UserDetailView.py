from django.conf import settings
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.permissions import IsAuthenticated
from CuidApp.models.user import User
from CuidApp.serializers.userSerializers import UserSerializer
from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser

class UserDetailView(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)
    


 





