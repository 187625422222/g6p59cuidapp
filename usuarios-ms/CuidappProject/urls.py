from django.contrib import admin
from django.urls import path
from django.urls import path
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from CuidApp import views
from django.contrib import admin
from django.urls import path

from django.conf.urls import url, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('verifyToken/', views.VerifyTokenView.as_view()),
    path('CreateUser/', views.UserCreateView.as_view()),
    path('ViewUser/', views.UserDetailView.as_view()),
    url(r'^',include('CuidApp.urls'))


]
